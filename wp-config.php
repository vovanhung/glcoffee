<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'glcoffee' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '*oLB1kVv5k.^):bYZd(^/((s$J`@u@SP>$?F6JQTkAZp`{0Ko;$],[kh15y47[&g' );
define( 'SECURE_AUTH_KEY',  '8M008.yQ#I=UVCu+{3Nx9|5G[)2?-rjW_lneutY>ZR(Hl1)amMmA.)Y{c@g*WG#v' );
define( 'LOGGED_IN_KEY',    'd_AcvM`d!BsSap~,l;cZ_Woo]^IK6z3a.gpb0&.JPZ3|%*<,>!4//D5*C]zoSSvw' );
define( 'NONCE_KEY',        '?09nK~Jfgl0R/p9TBP tV`0?WTp[2WX;K,;t(f&}( mss2JbZt >KkZck;EXoW6>' );
define( 'AUTH_SALT',        's:8yU 3h%NuE7hu{D@cqtC UNZJq*`-{*-igmMdH >XoHu,qo&zDf;Q3>2Ca,^J+' );
define( 'SECURE_AUTH_SALT', 'y<ip_mnx;!fy}YDn&O{q,IF_@]K_)Rm{443)CO&l ,u.%RF?STZ#FP*`?r;,A? n' );
define( 'LOGGED_IN_SALT',   'g|uQyvC0ij*k]~YvV^=FXm#)~PD n(`zme!j4g^)7`;w^>XJ:he[sqy;B()v1H)B' );
define( 'NONCE_SALT',       'Z`c|F5u|3kx_1^8>85aNyrG3;f*(Rz5V41yq],NAG`uigaVZQ_eq3,w3oR}aS3<F' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
