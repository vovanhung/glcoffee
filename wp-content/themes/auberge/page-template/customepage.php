<?php
/**
 * Custom page template
 *
 * Template Name: custom page
 *
 * @package    Auberge
 * @copyright  WebMan Design, Oliver Juhas
 *
 * @since    1.0
 * @version  2.0
 */

/* translators: Custom page template name. */
__( 'customepage', 'auberge' );




get_header();

while ( have_posts() ) : the_post();

if(!isset($_COOKIE[language_code])) {
	echo "The cookie: '" . language_code . "' is not set.";
  } else {
	echo "The cookie '" . language_code . "' is set.";
	echo "Cookie is:  " . $_COOKIE[language_code];
	$language_code = $_COOKIE[language_code];
	if($language_code == 'vn'){
		
get_template_part( 'template-parts/content.vn', 'page' );
	}
	else{
		get_template_part( 'template-parts/content.jp', 'page' );
	}
  }

endwhile;

get_sidebar();

get_footer();
